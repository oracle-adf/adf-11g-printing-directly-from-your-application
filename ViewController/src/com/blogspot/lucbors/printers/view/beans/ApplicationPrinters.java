package com.blogspot.lucbors.printers.view.beans;

import com.sun.faces.config.FacesConfigInfo;

import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.List;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.OrientationRequested;

import javax.print.attribute.standard.DocumentName;

import javax.print.attribute.standard.JobName;

import oracle.adf.view.rich.context.AdfFacesContext;


public class ApplicationPrinters {

  private String defaultPrinter;
  private String selectedPrinter;

  private SelectItem[] allPrinters;

private  String theFile;
  public ApplicationPrinters() {

  }

  public SelectItem[] getAllPrinters() {
    if (allPrinters == null) {
      PrintService[] printers =
        PrintServiceLookup.lookupPrintServices(null, null);
      allPrinters = new SelectItem[printers.length];
      for (int i = 0; i < printers.length; i++) {
        SelectItem printer =
          new SelectItem(printers[i].getName(), printers[i].getName());
        allPrinters[i] = printer;
      }
    }
    return allPrinters;
  }


  public void setDefaultPrinter(String defaultPrinter) {
    this.defaultPrinter = defaultPrinter;
  }

  public String getDefaultPrinter() {
    PrintService defaultPrinter =
      PrintServiceLookup.lookupDefaultPrintService();
    return defaultPrinter.getName();
  }


  public void setSelectedPrinter(String selectedPrinter) {
    this.selectedPrinter = selectedPrinter;

  }

  public String getSelectedPrinter() {
    if (selectedPrinter == null) {
      return getDefaultPrinter();
    } else {
      return selectedPrinter;
    }
  }

  public void setAllPrinters(SelectItem[] allPrinters) {
    this.allPrinters = allPrinters;
  }


  public void resetToDefault(ActionEvent actionEvent) {
    // Add event code here...
    setSelectedPrinter(getDefaultPrinter());
  }

  public void invokePrintJob(ActionEvent actionEvent) {
    // Add event code here...

    PrintService[] printers =
      PrintServiceLookup.lookupPrintServices(null, null);

    Boolean printerFound = false;
    for (int i = 0; i < printers.length && !printerFound; i++) {
      if (printers[i].getName().equalsIgnoreCase(getSelectedPrinter())) {
        printerFound=true;
        PrintService printer = printers[i];      
        String file = getTheFile();
        FileInputStream input;
        try {
          input =
              new FileInputStream(file);


          Doc doc = new SimpleDoc(input, DocFlavor.INPUT_STREAM.PNG, null);


          PrintRequestAttributeSet attrs = new HashPrintRequestAttributeSet();
          attrs.add(new Copies(2));
          attrs.add(OrientationRequested.LANDSCAPE);

          JobName name = new JobName(file ,null);
          attrs.add(name);
          
          DocPrintJob job = printer.createPrintJob();
          job.print(doc, attrs);
        } catch (PrintException e) {
          System.out.println(e.getMessage());

          addFacesErrorMessage(e.getMessage());

        } catch (FileNotFoundException e) {
          System.out.println(e.getMessage());
          addFacesErrorMessage(e.getMessage());
        }
      }
    }
  }

  public static void addFacesErrorMessage(String msg) {

    FacesContext fctx = getFacesContext();
    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, "");
    fctx.addMessage(getRootViewComponentId(), fm);
  }

  public static String getRootViewComponentId() {
    return getFacesContext().getViewRoot().getId();
  }

  public static FacesContext getFacesContext() {
    return FacesContext.getCurrentInstance();
  }

 
  public String getTheFile() {
    return "C:\\temp\\testFileForPrinting.txt";
  }
}
